FreeBSD
=======

General usage
-------------

pkg or how to work with binary packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It's pretty simple::

    # pkg update
    # pkg search vagrant
    vagrant-1.7.4_1
    # pkg install vagrant
    ...

Query installed packages::

    # pkg info

Remove packages::

    # pkg delete vagrant

Upgrade installed packages::

    # pkg upgrade

`more info <https://www.freebsd.org/doc/handbook/pkgng-intro.html>`_

the ports tree
~~~~~~~~~~~~~~

The ports tree is located in */usr/ports* as a simple directory structure.
To initially set up the ports direcotry, run ::

    # portsnap fetch extract

and to update::

    # portsnap fetch update

Installing ports is as simple as navigating to */usr/ports* and looking for the directory::

    # find /usr/ports -name vim
    /usr/ports/editors/vim
    # cd /usr/ports/editors/vim
    # make install clean
    ...

Removing ports::

    # cd /usr/ports/editors/vim
    # make deinstall

To look for updates use either *portsnap fetch update* ::

    # cd /usr/ports
    # make update

`more info <https://www.freebsd.org/doc/handbook/ports-using.html>`_

Little mishaps
--------------

So, you locked yourself out of your box again. How do we fix that?

Invalid shell on root account (don't change it, use the toor user, yada, yada..)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, boot to single-user mode and accept the default shell.

To get read/write access to your partitions::

    # mount -a
    # mount
    zroot/ROOT/default on / (zfs, local, noatime, readonly, nfsv4acls)
    # zfs set readonly=off zroot/ROOT

now run chsh for root::

    # chsh root

and fix the shell path. Welcome back!

